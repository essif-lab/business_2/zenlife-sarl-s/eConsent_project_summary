# eConsent Project Summary

For more information please contact:

Minh-Triet LE, lmt@zenlife.lu

# Problem

There is a global shortage of organ. The need is much higher than the offer, even in countries where citizens and residents are all presumed donors. Even if 80% of the population are willing to donate, 61% have never expressed their consent. In this case, medical staff approaches family to ask for consent and the refusal rate is high.

In addition, collected consents in the existing solutions have compliance issues. Consent extension module of EHR software vendor is too naive. Sometimes the consent is just a flag stored in the database. There is no way to prove the authenticity and the integrity of the consent. Public healthcare institutions are exposed to legal risks.

Institutions still use paper based process. It is impractical and incurs administrative burden. There is 2000 complains per year in Luxembourg on the average and file a court case takes several days.

# Solution

Zenlife eConsent is a **turn key solution** and we make it easy of manage the **whole life cycle** of consents and give them the **probative value**. 

- **Design** : Create dynamic consent form based on predefined schema
- **Distribute** : Certify the form and customize it with personal data before distribution. Re-consent can be acquired by creating a new version of the form and distribute it again
- **Collect and Deposit** : Users provide consent and certify it before deposit into the backend storage, that is used as the national donor registry
- **Demonstrate** : Deposited consents are protected as specified by legal electronical archiving ISO norm to give them probative value in complement to the validity of the digital signature. Blockchain is used in complement to witness the integrity of the internal journal of operation.

# Value Proposition

- **Public Health Authority** : Build efficient governance across involved parties
- **Citizens** : Empower citizen in an ethical relationship
- **Doctors** : Liabilities protection
- **Legal** : Instant compliance demonstration
- **IT operation** : integrated turn key solution, easy to manage and to integrate into a enterprise grade environment